pragma solidity ^0.5.0;

interface STToken {
    function decimals() external view returns(uint8);
    function balanceOf(address _address) external view returns(uint256);
    function transfer(address _to, uint256 _value) external returns  (bool success);
}

contract STSale {
    address owner;
    uint256 price;
    STToken stTokenContract;
    uint256 tokenSold;

    event Sold(address buyer, uint256 amount);

    constructor(uint256 _price, address _addressContract) public {
        owner = msg.sender;
        price = _price;
        stTokenContract = STToken(_addressContract);
    }

    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        if (a == 0){
            return 0;
        }
        uint256 c = a * b;
        require(c / a == b);
        return c;
    }

    function buy(uint256 _numTokens) public payable {
        require(msg.value == mul(price, _numTokens));
        uint256 scaledAmount = mul(_numTokens, uint256(10) ** stTokenContract.decimals());
        require(stTokenContract.balanceOf(address(this)) >= scaledAmount );
        tokenSold += _numTokens;
        require(stTokenContract.transfer(msg.sender, scaledAmount));
        emit Sold(msg.sender, _numTokens);
    }

    function endSold() public {
        require(msg.sender == owner);
        require(stTokenContract.transfer(owner, stTokenContract.balanceOf(address(this))));
        msg.sender.transfer(address(this).balance);
    }
}